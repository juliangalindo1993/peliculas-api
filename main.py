from fastapi import FastAPI, HTTPException, Request
from fastapi.security import HTTPBearer
#from middlewares import JWTBearer
from fastapi.responses import HTMLResponse, JSONResponse
from pydantic import BaseModel
from config.database import engine, Base
#from models.movie import Movie as MovieModel
#from fastapi.encoders import jsonable_encoder
from middlewares.error_handler import ErrorHandler
from middlewares import jwt_bearer
from fastapi import Request, HTTPException
from routers.movie import movie_router
from routers.user import user_router
from utils.jwt_manager import create_token, validate_token



app = FastAPI()
app.title = 'mi aplicacion con FastAPI'
app.version = "0.0.1"
 
app.add_middleware(ErrorHandler)
app.include_router(movie_router)
app.include_router(user_router)

Base.metadata.create_all(bind=engine)

class JWTBearer(HTTPBearer):
    async def __call__(self, request: Request):
        auth = await super().__call__(request)
        data = validate_token(auth.credentials)
        if data["email"] != "admin@gmail.com":
            #return request
            raise HTTPException(status_code=403, detail="crendenciales invalidas")
        
class user(BaseModel):
    email:str
    password:str

movies = [
    {
    "id": 1,
    "title": "Avatar",
    "overview": "en un exuberante planeta llamado pandora viven los na'vi",
    "year": "2009",
    "rating": 7.9,
    "category": "Accion"
    },
    {"id": 2,
    "title": "Hongos maravillosos",
    "overview": "en un exuberante planeta llamado pandora viven los na'vi",
    "year": "2012",
    "rating": 9,
    "category": "Documental"
    }
]

#rutas en las que aparece en el localhost.

@app.get('/', tags=['home'])
def message():
    return HTMLResponse('<h1>Hello world</h1>')

@app.post('/login', tags=['auth'])
def login(user: user):
    if user.email == "admin@gmail.com" and user.password == "admin":
        token: str = create_token(user.dict())
        return JSONResponse(status_code=200, content=token)

