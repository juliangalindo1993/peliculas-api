from pydantic import BaseModel, Field
from typing import Optional, List


class Movie(BaseModel):
    id: Optional[int] = None
    title: str = Field(min_length=3, max_length=20)
    overview: str = Field(min_length=3, max_length=50)
    year : Optional[int] = Field(le=2022)
    rating: float = Field(ge=1, le=10)
    category: str = Field(min_length=5, mas_length=20)
    
    class Config:
        json_schema_extra = {
            "example": {
                "id": 1,
                "title": "Mi pelicula",
                "overview": "Descripcion de la pelicula",
                "year": 2022,
                "rating": 9.9,
                "category": "Accion"
            }
        }

